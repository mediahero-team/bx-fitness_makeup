$(document).ready(function() {
	console.log('jquery ready!');
	console.log('document ready!');
	
	$('.js-bx-slider').bxSlider();

	function addSliderHeight () {
		var sliderHeight = $('.main-slider').outerHeight();
		$('.js-bx-captions').css('height', sliderHeight);
	}

	addSliderHeight();

	$(window).resize(function () {
		addSliderHeight();
	});
});